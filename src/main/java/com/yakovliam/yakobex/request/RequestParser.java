package com.yakovliam.yakobex.request;

import com.yakovliam.yakobex.middleware.MiddleWareManager;
import com.yakovliam.yakobex.serverutil.Response;
import com.yakovliam.yakobex.util.Pair;

public class RequestParser {

    public static Response parse(Request request) {

        Pair<Response, Boolean> pair = new MiddleWareManager().validate(request); // go through ALL middleWares to make sure it's correct & a good request

        return pair.getA();
    }
}
