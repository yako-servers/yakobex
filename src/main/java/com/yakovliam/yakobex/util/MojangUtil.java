package com.yakovliam.yakobex.util;

import com.yakovliam.yakobex.serverutil.Response;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;

import static com.yakovliam.yakobex.serverutil.Response.NO_ERRORS;

public class MojangUtil {

    public static Pair<Response, Boolean> exists(UUID uuid) {

        Triplet<Response, Boolean, JSONObject> data = getData(uuid);

        return new Pair<>(data.getA(), data.getB());
    }

    public static Triplet<Response, Boolean, String> playerName(UUID uuid) {

        Triplet<Response, Boolean, JSONObject> data = getData(uuid);

        if (data.getC() == null) {
            return new Triplet<>(Response.INVALID_UUID, data.getB(), null);
        }

        // name history
        String name;
        try {
            name = data.getC().getJSONObject("data").getJSONObject("player").getString("username");
        } catch (Exception ignored) {
            return new Triplet<>(Response.INVALID_UUID, data.getB(), null);
        }

        // get first item
        if (name != null)
            return new Triplet<>(NO_ERRORS, true, name);
        else {
            return new Triplet<>(Response.INVALID_UUID, data.getB(), null);
        }
    }

    private static Triplet<Response, Boolean, JSONObject> getData(UUID uuid) {
        String urlString = "https://playerdb.co/api/player/minecraft/" + uuid.toString();

        Response respObj;
        boolean success;
        JSONObject jsonObject = null;

        try {
            URL obj = new URL(urlString);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.addRequestProperty("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/80.0.3987.149 Chrome/80.0.3987.149 Safari/537.36");
            con.addRequestProperty("method", "GET");
            con.setReadTimeout(5000);
            con.setConnectTimeout(5000);

            int responseCode = con.getResponseCode();

            if (responseCode == 500) { // invalid request (nonexistent)
                throw new Exception("Invalid Minecraft Account UUID!");
            }

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));

            StringBuilder response = new StringBuilder();
            in.lines().forEach(response::append);

            in.close();

            JSONObject json = new JSONObject(response.toString());

            jsonObject = json;

            if (json.has("error") && json.getBoolean("error")) {
                throw new Exception("Invalid Minecraft Account UUID!");
            } else if (json.has("success") && json.getBoolean("success")) {
                respObj = NO_ERRORS;
                success = true;
            } else {
                throw new Exception("Invalid Minecraft Account UUID!");
            }

        } catch (Exception e) {
            respObj = Response.INVALID_UUID;
            success = false;
        }

        return new Triplet<>(respObj, success, jsonObject);
    }
}