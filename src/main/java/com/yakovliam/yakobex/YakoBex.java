package com.yakovliam.yakobex;

import com.yakovliam.yakobex.config.YakoBexConfig;
import com.yakovliam.yakobex.net.JavalinServer;
import com.yakovliam.yakocore.api.YakoPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class YakoBex extends YakoPlugin {

    private static YakoBex instance;
    private static YakoBexConfig config;
    private static boolean debug = true;

    public static YakoBexConfig getYakoBexConfig() {
        return config;
    }

    public static YakoBex getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;

        // load config
        config = new YakoBexConfig();

        initServer();
    }

    private void initJavalin() {
        new JavalinServer().start();
    }


    private void initServer() {
        /* SWAP CLASS LOADER */

        // Because Bukkit/Spigot/Bungeecord uses a custom class loader per plugin, Javalin is unable
        // to start unless it is instantiated using the plugin's own class loader.

        // Get the current class loader.
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        // Temporarily set this thread's class loader to the plugin's class loader.
        // Replace JavalinTestPlugin.class with your own plugin's class.
        Thread.currentThread().setContextClassLoader(YakoBex.class.getClassLoader());

        // Instantiate the web server (which will now load using the plugin's class loader).
        initJavalin();

        // Put the original class loader back where it was.
        Thread.currentThread().setContextClassLoader(classLoader);
    }

    public static void debug(String message) {
        if (debug)
            Bukkit.getConsoleSender().sendMessage(ChatColor.GOLD + getInstance().getDescription().getFullName() + " Debug > " + ChatColor.WHITE + ChatColor.translateAlternateColorCodes('&', message));
    }


}
