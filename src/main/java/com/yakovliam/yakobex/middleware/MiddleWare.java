package com.yakovliam.yakobex.middleware;

import com.yakovliam.yakobex.request.Request;
import com.yakovliam.yakobex.serverutil.Response;

public abstract class MiddleWare {

    public String job;
    public boolean cont;

    public boolean isCont() {
        return cont;
    }

    public void setCont(boolean cont) {
        this.cont = cont;
    }

    public String getJob() {
        return job;
    }

    public abstract Response validate(Request request);

    public boolean isContinue() {
        return this.cont;
    }

}
