package com.yakovliam.yakobex.request;

import java.util.UUID;

public class Request {

    private String packageName;
    private RequestType type;
    private String subType;
    private UUID uuid;
    private String value;

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public RequestType getType() {
        return type;
    }

    public void setType(RequestType type) {
        this.type = type;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Request(RequestType type, String subType, UUID uuid, String value, String packageName) {
        this.type = type;
        this.subType = subType;
        this.uuid = uuid;
        this.value = value;
        this.packageName = packageName;
    }

    public Request() {

    }
}
