package com.yakovliam.yakobex.middleware;

import com.yakovliam.yakobex.request.Request;
import com.yakovliam.yakobex.serverutil.Response;
import com.yakovliam.yakobex.util.MojangUtil;
import com.yakovliam.yakobex.util.Triplet;
import org.bukkit.ChatColor;

import static com.yakovliam.yakobex.util.Messages.PURCHASED;

public class BroadcastMiddleWare extends MiddleWare {

    @Override
    public Response validate(Request request) {

        // get player name from mojang util
        Triplet<Response, Boolean, String> data = MojangUtil.playerName(request.getUuid());

        if (!data.getB() || data.getC() == null)
            return data.getA();

        PURCHASED.broadcast("%name%", data.getC(), "%packageName%", ChatColor.translateAlternateColorCodes('&', request.getPackageName()));

        return Response.NO_ERRORS;
    }
}
