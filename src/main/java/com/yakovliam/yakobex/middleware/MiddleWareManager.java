package com.yakovliam.yakobex.middleware;

import com.yakovliam.yakobex.request.Request;
import com.yakovliam.yakobex.serverutil.Response;
import com.yakovliam.yakobex.util.Pair;

import java.util.*;

public class MiddleWareManager {

    private List<MiddleWare> middleWareList = new ArrayList<>();

    public MiddleWareManager() {
        this.middleWareList.addAll(Arrays.asList(
                new UUIDMiddleWare(),
                new ParserMiddleWare(),
                new BroadcastMiddleWare()
        ));
    }

    public Pair<Response, Boolean> validate(Request request) {
        boolean succession = true;
        Iterator<MiddleWare> iterator = middleWareList.iterator();
        Response lastResponse = Response.NO_ERRORS;
        // run through middleWares
        while (succession && iterator.hasNext()) {
            MiddleWare middleWare = iterator.next();

            lastResponse = middleWare.validate(request);
            succession = middleWare.isCont();
        }

        return new Pair<>(lastResponse, succession);
    }

}
