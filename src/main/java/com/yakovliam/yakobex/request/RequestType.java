package com.yakovliam.yakobex.request;

public enum RequestType {

    COSMETIC,
    PERMISSION;

    public static RequestType getRequestType(String string) {
        return valueOf(string.toUpperCase());
    }

}
