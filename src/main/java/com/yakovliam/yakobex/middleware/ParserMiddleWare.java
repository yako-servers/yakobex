package com.yakovliam.yakobex.middleware;

import com.yakovliam.yakobex.YakoBex;
import com.yakovliam.yakobex.function.RequestFunction;
import com.yakovliam.yakobex.request.Request;
import com.yakovliam.yakobex.request.RequestType;
import com.yakovliam.yakobex.serverutil.Response;

import java.util.UUID;

public class ParserMiddleWare extends MiddleWare {

    @Override
    public Response validate(Request request) {

        RequestType requestType = request.getType();
        String subType = request.getSubType();
        UUID uuid = request.getUuid();
        String value = request.getValue();

        Response response = Response.NO_ERRORS;
        boolean success = false;

        switch (requestType) {
            case COSMETIC:
                switch (subType) {
                    case "title":
                        success = RequestFunction.giveCosmetic(uuid, "title", value);
                        break;
                    case "namecolor":
                        success = RequestFunction.giveCosmetic(uuid, "namecolor", value);
                        break;
                    case "chatcolor":
                        success = RequestFunction.giveCosmetic(uuid, "chatcolor", value);
                        break;
                    default:
                        response = Response.INVALID_SUBTYPE;
                        break;
                }
                break;
            case PERMISSION:
                YakoBex.debug("&fGave permission node &c" + request.getValue() + "&f to &c" + uuid.toString());
                break;
            default:
                response = Response.INVALID_TYPE;
                break;
        }

        cont = success;
        return response;
    }
}
