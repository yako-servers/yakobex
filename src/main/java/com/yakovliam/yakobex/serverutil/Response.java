package com.yakovliam.yakobex.serverutil;

import org.json.JSONObject;

public enum Response {

    // invalid
    INVALID_JSON("Invalid json"),
    INVALID_TOKEN("Invalid token"),
    INVALID_PARAMETERS("Invalid parameters"),

    ERROR("Error"),
    UUID_ERROR("UUID error!"),
    INVALID_UUID("Invalid Minecraft Account UUID!"),

    INVALID_SUBTYPE("Invalid subType!"),
    INVALID_TYPE("Invalid type!"),

    // all good
    NO_ERRORS("No errors");

    String message;

    Response(String message) {
        this.message = message;
    }

    public JSONObject getJson() {
        return new JsonResponse().parseToResponse(this.message);
    }

    public String getString() {
        return new JsonResponse().parseToResponse(this.message).toString();
    }

    public static JSONObject getJson(String message) {
        return new JsonResponse().parseToResponse(message);
    }

    public static String getString(String message) {
        return new JsonResponse().parseToResponse(message).toString();
    }
}
