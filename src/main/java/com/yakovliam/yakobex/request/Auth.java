package com.yakovliam.yakobex.request;

import com.yakovliam.yakobex.YakoBex;
import com.yakovliam.yakobex.config.YakoBexConfig;

public class Auth {

    public static boolean isAuthenticated(String reqAuthId, String reqAuthToken) {

        // get config
        YakoBexConfig config = YakoBex.getYakoBexConfig();

        String authId = config.getConfig().getString("auth-id");
        String authToken = config.getConfig().getString("auth-token");

        if (reqAuthId == null || reqAuthToken == null || authId == null || authToken == null) {
            return false;
        }

        if (reqAuthId.equals(authId) && reqAuthToken.equals(authToken)) {
            return true;
        }

        return false;
    }


}
