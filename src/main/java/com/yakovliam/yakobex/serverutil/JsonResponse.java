package com.yakovliam.yakobex.serverutil;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class JsonResponse {

    public JSONObject parseToResponse(String message) {

        Map<String, String> responseMap = new HashMap<>();

        responseMap.put("response", message);

        return new JSONObject(responseMap);
    }
}
