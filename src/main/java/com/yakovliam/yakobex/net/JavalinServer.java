package com.yakovliam.yakobex.net;

import com.yakovliam.yakobex.YakoBex;
import com.yakovliam.yakobex.request.Auth;
import com.yakovliam.yakobex.request.Request;
import com.yakovliam.yakobex.request.RequestParser;
import com.yakovliam.yakobex.request.RequestType;
import com.yakovliam.yakobex.serverutil.Response;
import io.javalin.Javalin;
import io.javalin.http.Context;
import org.json.JSONObject;

import java.util.UUID;

import static com.yakovliam.yakobex.serverutil.Response.*;

public class JavalinServer extends Thread {

    private Javalin app;

    @Override
    public void start() {

        app = Javalin.create().start(8080);

        app.post("/purchase", ctx -> {
            ctx.contentType("application/json"); // set context responseType = application/json

            YakoBex.debug("&fReceived request from &c" + ctx.ip());

            String body = ctx.body(); // get body from post

            System.out.println("Body > " + body);

            JSONObject bodyJson; // json body

            try {
                bodyJson = new JSONObject(body);
            } catch (Exception ignored) {
                invalid(ctx, INVALID_JSON.getJson());
                return;
            }

            /* get token from request */

            boolean isAuthenticated = false;

            String reqAuthId;
            String reqAuthToken;

            try {
                reqAuthId = bodyJson.getString("auth-id");
                reqAuthToken = bodyJson.getString("auth-token");

                isAuthenticated = Auth.isAuthenticated(reqAuthId, reqAuthToken); // authenticate token and iD
            } catch (Exception ignored) {
            }

            if (!isAuthenticated) {
                invalid(ctx, INVALID_TOKEN.getJson());
                return;
            }

            /* authenticated */

            // try to build a request
            Request request = new Request();

            try {
                // request type
                RequestType type = RequestType.getRequestType(bodyJson.getString("reqType"));

                if (type == null)
                    throw new Exception("C:Invalid request type");

                request.setType(type);

                request.setSubType(bodyJson.getString("subType"));

                request.setUuid(UUID.fromString(bodyJson.getString("uuid")));

                request.setValue(bodyJson.getString("value"));

                request.setPackageName(bodyJson.getString("packageName"));

            } catch (Exception e) {
                if (e.getMessage().startsWith("C:")) {
                    invalid(ctx, Response.getJson(e.getMessage()));
                    return;
                }
                invalid(ctx, INVALID_PARAMETERS.getJson());
                return;
            }

            // parse request
            Response response = RequestParser.parse(request);

            YakoBex.debug("&fParsed request, sending response: &b" + response.toString());

            ctx.result(response.getString());
            ctx.status(200);
        });
    }

    private void invalid(Context context, JSONObject response) {
        context.status(400);
        context.result(response.toString());
    }

}
