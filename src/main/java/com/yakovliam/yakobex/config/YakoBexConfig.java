package com.yakovliam.yakobex.config;

import com.yakovliam.yakobex.YakoBex;
import com.yakovliam.yakocore.api.config.Config;
import org.bukkit.plugin.Plugin;

public class YakoBexConfig extends Config {

    public YakoBexConfig() {
        super(YakoBex.getInstance(), "config.yml");
    }
}
