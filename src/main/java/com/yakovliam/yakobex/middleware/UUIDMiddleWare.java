package com.yakovliam.yakobex.middleware;

import com.yakovliam.yakobex.request.Request;
import com.yakovliam.yakobex.serverutil.Response;
import com.yakovliam.yakobex.util.MojangUtil;
import com.yakovliam.yakobex.util.Pair;

public class UUIDMiddleWare extends MiddleWare {


    @Override
    public Response validate(Request request) {
        // validate that uuid exists
        Pair<Response, Boolean> pair = MojangUtil.exists(request.getUuid());

        cont = pair.getB();
        return pair.getA();
    }

    @Override
    public boolean isContinue() {
        return cont;
    }
}
