package com.yakovliam.yakobex.function;

import com.yakovliam.yakobex.YakoBex;
import com.yakovliam.yakocore.api.permissions.LuckPermsAPI;
import com.yakovliam.yakocore.util.cosmetic.CosmeticsUtil;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.model.user.User;
import net.luckperms.api.model.user.UserManager;
import net.luckperms.api.node.Node;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class RequestFunction {

    public static boolean giveCosmetic(UUID uuid, String cosmeticType, String cosmeticName) {
        String node = CosmeticsUtil.getPermissionNodeFromCosmetic(cosmeticType, cosmeticName);

        return giveNode(uuid, node);
    }

    private static boolean giveNode(UUID uuid, String node) {
        // give node
        LuckPerms api = LuckPermsAPI.getLuckPermsAPI();

        UserManager userManager = api.getUserManager();
        CompletableFuture<User> userFuture = userManager.loadUser(uuid);

        userFuture.thenAcceptAsync(user -> {
            user.data().add(Node.builder(node).build());
            YakoBex.debug("&fGave permission node &c" + node + "&f to &c" + uuid.toString());
            api.getUserManager().saveUser(user);
        });

        return true;
    }

}
